(require 'mothtime)
(require 'mothint)

(defun mothatpt--operate-thing (int-operation time-operation arg)
  (let ((increment (or arg 1)))
    (cond
     ((condition-case nil
          (if (thing-at-point 'time)
              (funcall time-operation arg))
        (error nil)))
     ((condition-case nil
          (if (thing-at-point 'integer)
              (funcall int-operation arg))
        (error nil))))))

(defun mothatpt-increment (&optional arg)
  (interactive "p")
  (let ((increment (or arg 1)))
    (mothatpt--operate-thing
     'mothint-increment-number-at-point
     'mothtime--increment-minutes-time-at-point
     arg)))

(defun mothatpt-decrement (&optional arg)
  (interactive "p")
  (let ((increment (or arg 1)))
    (mothatpt--operate-thing
     'mothint-decrement-number-at-point
     'mothtime--decrement-minutes-time-at-point
     arg)))


(provide 'mothatpt)
